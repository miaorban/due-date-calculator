/*
I store my custom error messages in this file
so if I wanted to change any of them I have to update the text only here.

I used these variables in the test files and the classes as well
*/

module.exports = {
  created_at_is_invalid_date: 'Created at is invalid date',
  created_at_cannot_be_fututre_date: 'Created at cannot be future date',
  created_at_must_be_during_business_hours: 'Created at must be during business hours (9AM to 5PM, weekdays only)',
  turnaround_time_must_be_positive_whole_number: 'Turnaround time must be a positive whole number'
}