# Due date calculator

Overview: this project implements a **due date calculator for an issue tracking system**.
As input, it takes the date when the issue was submitted and the turnarond tim (the amount of hours it takes to resolve the issue).
It outputs the date when the issue will be resolved.

## Usage (index.js)

```
const Issue = require('./classes/Issue')

// caluculate due date using the static calculateDueDate method
const dueDate = Issue.calculateDueDate('2020. 02. 28. 16:00, 2)

// caluculate due date by creating an Issue instance
const issue = new Issue('2020. 02. 28. 16:00', 2)
console.log('due date', issue.dueDate) // '2020. 03. 02. 10:00'
```



## Description

In this project I implemented the **Issue class** which **constructor** takes two parameters:
| constructor | type | rules | description
| ------ | ------ | ------ | ------ |
| submission time | dateTime/number of milliseconds* since the [Unix Epoch](https://en.wikipedia.org/wiki/Unix_time) | Must be past date and must be during business hours. I will talk about business hours later. | The time when the issue was submitted. |
| turnarond time | positive whole number | Must be greater than 0 | The amount of working hours to takes to resolve the issue. |

Static methods, properties, getters in the Issue class:
| name | type | value type | description
| ------ | ------ | ------ | ------ |
| createdAt | property | whole number | The time when the issue was submitted. Shows the number of milliseconds since the Unix Epoch |
| turnaroundTime | property |  whole number | The amount of hours to takes to resolve the issue. |
| dueDate | getter | string representing a specified Date object | The date when the issue will be resolved |
| calculateDueDate | static method | string representing a specified Date object | The date when the issue will be resolved. Does exactly the same as dueDate getter but you can call it on the Issue class. |

## Business hours

As mentioned before, the calculator will ignore weekends an non-working hours during the week when caluclating due date. Current settings:
| day | start time | end time |
| ------ | ------ | ------ |
| monday | 09:00 | 17:00 |
| tuesday | 09:00 | 17:00 |
| wednesday | 09:00 | 17:00 |
| thursday | 09:00 | 17:00 |
| friday | 09:00 | 17:00 |
| saturday | null | null |
| sunday | null | null |

These settings can be overriden only by modifying the source code. The details are specified in the Issue.js file, look for the **businessWeek** property.

## Getting Started

### Dependencies

* [Node](https://nodejs.org/en/download/) >=12
* Mocha, chai for testing

### Installing

* Open terminal or command prompt
* Clone this directory
* Cd into the project folder (due-date-caluculator if you didn't rename it)
* run npm i (this will install mocha and chai, so you can run the tests)

### Executing program

* run: `node index.js`
* test: `npm test`
```
code blocks for commands
```

## About the tests
Basically I wrote tests for the inputs given by the users and the due dates in specific cases.
The user inputs are the parameters the constructor function awaits.

## Authors

Mia Orbán. miaorban@gmail.com

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the [MIT] License - see the LICENSE.md file for details
