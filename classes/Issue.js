const validationErrorMessages = require('../errors/validationErrors')

Date.prototype.addHours = function(h) {
  this.setTime(this.getTime() + (h*60*60*1000));
  return this;
}

// must be given in HH:MM format
const BUSINESS_HOURS = {
  start: '09:00',
  end: '17:00',
}

/*
The JS's getDay() method returns the day of the week (from 0 to 6) for the specified date.
I used these arrays to convert it's output to readable day names.
(I think it is easier to read day names than integers)
*/
const DAYS = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

class Issue {
  constructor (createdAt, turnaroundTime) {
    this.validateCreatedAt(createdAt)
    this.validateTurnaroundTime(turnaroundTime)
    this.createdAt = new Date(createdAt).getTime()
    this.turnaroundTime = turnaroundTime
  }
  
  // this is the getter. this one should be looked up when wanting to establish the due date
  get dueDate () {
    return this.calculateDueDate(this.createdAt, this.turnaroundTime)
  }
  
  // this is the method that does the calculation
  calculateDueDate (createdAt, turnaroundTime) {
    let turnaroundTimeCopy = turnaroundTime
    let startTime = createdAt
    while (turnaroundTimeCopy) {
      startTime = this.addHour(startTime)
      if (this.isInBusinessHours(startTime)) {
        turnaroundTimeCopy--
      }
    }
    return startTime.toString()
  }
  
  addHour (time, hours = 1) {
    return new Date(time).addHours(hours)
  }

  isInBusinessHours (time) {
    if (!this.isInBusinessDay(time)) return false

    const daysBusinessHours = this.getDaysBusinessHours(time)    
    const [businessHoursStartHours, businessHoursStartMinutes] = daysBusinessHours.start.split(':')
    const [businessHoursEndHours, businessHoursEndMinutes] = daysBusinessHours.end.split(':')
    const businessHoursStartMins = businessHoursStartHours * 60 + parseInt(businessHoursStartMinutes)
    const businessHoursEndMins = businessHoursEndHours * 60 + parseInt(businessHoursEndMinutes)
    
    const hours = new Date(time).getHours()
    const minutes = new Date(time).getMinutes()
    const currentMins = hours * 60 + minutes

    return (businessHoursStartMins < currentMins) &&
      (currentMins <= businessHoursEndMins)
  }

  getDaysBusinessHours (time) {
    const dayNumber = new Date(time).getDay()
    const dayName = DAYS[dayNumber]
    return this.businessWeek[dayName]
  }

  isInBusinessDay (time) {
    const dayNumber = new Date(time).getDay()
    const dayName = DAYS[dayNumber]
    return !!this.businessWeek[dayName]
  }

  /*
  create a static method as well, so it's not nesseccary to create an Issue instance 
  if we want to to calculate due date
  */
  static calculateDueDate (createdAt, turnaroundTime) {
    const issue = new Issue(createdAt, turnaroundTime)
    return issue.dueDate
  }

  validateCreatedAt (val) {    
    const d = new Date(val)

    if (d.getTime() > new Date().getTime()) 
      throw new Error(validationErrorMessages.created_at_cannot_be_fututre_date)

    if (!(Object.prototype.toString.call(d) === '[object Date]' && isFinite(d))) 
      throw new Error(validationErrorMessages.created_at_is_invalid_date)

    if (!this.isInBusinessHours(val))
      throw new Error(validationErrorMessages.created_at_must_be_during_business_hours)
  }
  
  validateTurnaroundTime (val) {
    const floatN = parseFloat(val)
    if (!(!isNaN(floatN) && isFinite(val) && floatN > 0 && floatN % 1 == 0))
      throw new Error(validationErrorMessages.turnaround_time_must_be_positive_whole_number)
  }

  /*
  it can be readjusted if you have custom working hours.
  use any falsy values if a day is a non-working day
  */
  businessWeek = {
    monday: BUSINESS_HOURS,
    tuesday: BUSINESS_HOURS,
    wednesday: BUSINESS_HOURS,
    thursday: BUSINESS_HOURS,
    friday: BUSINESS_HOURS,
    saturday: null,
    sunday: false
  }
}

module.exports = Issue
