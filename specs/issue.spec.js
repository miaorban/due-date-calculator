'use strict'

const validationErrorMessages = require('../errors/validationErrors')
const Issue = require('../classes/Issue')
const expect = require('chai').expect

describe('Issue class', () => {
  describe('constructor', () => {
    describe('createdAt should only accept valid date or time', () => {
      it("doesn't accept string", () => {
        expect(() => new Issue('aa', 1)).to.throw(validationErrorMessages.created_at_is_invalid_date);
      })

      it("doesn't accept function", () => {
        expect(() => new Issue(() => 'hello'), 1).to.throw(validationErrorMessages.created_at_is_invalid_date);
      }) 

      it("doesn't accept object", () => {
        expect(() => new Issue({ hello: 'alma' }, 1)).to.throw(validationErrorMessages.created_at_is_invalid_date);
      })
      
      it("doesn't accept invalid date", () => {
        expect(() => new Issue('2015.03.41', 1)).to.throw(validationErrorMessages.created_at_is_invalid_date);
      }) 

      it("doesn't accept future date", () => {
        const futureTime = new Date().getTime() + (10 * 86400000)
        expect(() => new Issue(futureTime, 1)).to.throw(validationErrorMessages.created_at_cannot_be_fututre_date);
      }) 
      
      it("doesn't accept null", () => {
        expect(() => new Issue(null, 1)).to.throw(validationErrorMessages.created_at_must_be_during_business_hours);
      })
      
      it("accepts time as a number", () => {
        const d = new Date('2021.06.25. 15:00').getTime()
        const issue = new Issue(d, 1)
        expect(issue.createdAt).to.be.a('number').and.equal(d);
      })

      it('accepts date object', () => {
        const issue = new Issue('2021.04.12. 13:30', 1)
        expect(issue.createdAt).to.be.a('number').and.equal(new Date('2021.04.12. 13:30').getTime())  
      })

      it('accepts only business hours (9AM to 5PM, weekdays only)', () => {
        expect(() => new Issue('2021.06.20. 16:00', 1)).to.throw(validationErrorMessages.created_at_must_be_during_business_hours);
        expect(() => new Issue('2021.06.21. 19:00', 1)).to.throw(validationErrorMessages.created_at_must_be_during_business_hours);
        expect(() => new Issue('2021.06.21. 17:01', 1)).to.throw(validationErrorMessages.created_at_must_be_during_business_hours);
        expect(() => new Issue('2021.06.21. 17:00', 1)).to.not.throw();
        expect(() => new Issue('2021.06.21. 15:59', 1)).to.not.throw();
      })
    })

    describe('turnaroundTime should only accept positive integers and zero', () => {
      it("doesn't accept string", () => {
        expect(() => new Issue('2021.04.12. 13:30', 'test')).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number);
      })

      it("doesn't accept function", () => {
        expect(() => new Issue('2021.04.12. 13:30', () => 'hello')).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number);
      }) 

      it("doesn't accept object", () => {
        expect(() => new Issue('2021.04.12. 13:30', { hello: 'alma' })).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number);
      })
      
      it("doesn't accept negative integer", () => {
        expect(() => new Issue('2021.04.12. 13:30', -1)).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number);
      })

      it("doesn't accept double", () => {
        expect(() => new Issue('2021.04.12. 13:30', 1.2)).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number);
      })
      
      it("doesn't accept zero", () => {
        expect(() => new Issue('2021.04.12. 13:30', 0)).to.throw(validationErrorMessages.turnaround_time_must_be_positive_whole_number) 
      })

      it('accepts positive integer', () => {
        const issue = new Issue('2021.04.12. 13:30', 1)
        expect(issue.turnaroundTime).to.be.a('number').and.equal(1) 
      })
    })
  })

  describe('dueDate getter',  () => {
    it('should add working hours within the same business day', () => {
      const issue = new Issue('2021. 06. 14. 09:01', 3) 
      const expectedDueDate =  new Date('2021. 06. 14. 12:01').toString()
      expect(issue.dueDate).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should add working hours and return the next business day', () => {
      const issue = new Issue('2021. 06. 14. 09:01', 10) 
      const expectedDueDate =  new Date('2021. 06. 15. 11:01').toString()
      expect(issue.dueDate).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should handle weekends', () => {
      const issue = new Issue('2021. 06. 11. 09:01', 10) 
      const expectedDueDate =  new Date('2021. 06. 14. 11:01').toString()
      expect(issue.dueDate).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should handle years', () => {
      const issue = new Issue('2020. 12. 31. 09:01', 10) 
      const expectedDueDate =  new Date('2021. 01. 01. 11:01').toString()
      expect(issue.dueDate).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should handle leap days', () => {
      const issue = new Issue('2020. 02. 28. 16:00', 2) 
      const expectedDueDate =  new Date('2020. 03. 02. 10:00').toString()
      expect(issue.dueDate).to.be.a('string').and.equal(expectedDueDate)
    })
  })

  describe('dueDate static method',  () => {
    it('should add working hours within the same business day', () => {
      const expectedDueDate =  new Date('2021. 06. 14. 12:15').toString()
      expect(Issue.calculateDueDate('2021. 06. 14. 09:15', 3)).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should add working hours and return the next business day', () => {
      const expectedDueDate =  new Date('2021. 06. 15. 11:01').toString()
      expect(Issue.calculateDueDate('2021. 06. 14. 09:01', 10)).to.be.a('string').and.equal(expectedDueDate)
    })
    
    it('should handle weekends', () => {
      const expectedDueDate =  new Date('2021. 06. 14. 11:01').toString()
      expect(Issue.calculateDueDate('2021. 06. 11. 09:01', 10)).to.be.a('string').and.equal(expectedDueDate)
    })
    
    it('should handle years', () => {
      const expectedDueDate =  new Date('2021. 01. 01. 11:01').toString()
      expect(Issue.calculateDueDate('2020. 12. 31. 09:01', 10)).to.be.a('string').and.equal(expectedDueDate)
    })

    it('should handle leap days', () => {
      const expectedDueDate =  new Date('2020. 03. 02. 10:00').toString()
      expect(Issue.calculateDueDate('2020. 02. 28. 16:00', 2)).to.be.a('string').and.equal(expectedDueDate)
    })
  })
})